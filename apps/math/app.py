#!/usr/bin/env python3

import os
from flask import Flask, jsonify
from datetime import datetime

app = Flask(__name__)

@app.route('/')
def homepage():
    return jsonify({
        'message': "hello world"
    })

@app.route('/sum/<a>/<b>')
def sum(a, b):
    return str(int(a) + int(b))

@app.route('/sub/<a>/<b>')
def sub(a, b):
    return str(int(a) - int(b))

@app.route('/time')
def time():
    today = datetime.now()

    return today.strftime('%Y-%m-%d %H:%M:%S')


if __name__ == '__main__':
    app.run(
        host=os.getenv('FLASK_HOST', '127.0.0.1'),
        port=os.getenv('FLASK_POST', '5000'),
        debug=bool(os.getenv('FLASK_DEBUG', False))
    )
