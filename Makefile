.PHONY: flask

flask:
	docker build -f docker/flask/Dockerfile -t registry.gitlab.com/forteil/sandbox/flask .
	docker push registry.gitlab.com/forteil/sandbox/flask

test:
	docker run \
	--rm \
	-e FLASK_HOST='0.0.0.0' \
	-p 5000:5000 \
	-t registry.gitlab.com/forteil/sandbox/flask
